export const getProfile = () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve({
        firstName: 'Mariano',
        lastName: 'Rasgado',
        avatar: 'https://picsum.photos/id/237/200/200'
      });
    }, 1000);
  });
};
