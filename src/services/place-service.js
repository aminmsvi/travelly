const data = [
  {
    id: 1,
    image: 'http://lorempixel.com/320/480/city/1',
    town: 'Bali',
    country: 'Indonesia',
    isFavorite: false
  },
  {
    id: 2,
    image: 'http://lorempixel.com/320/480/city/2',
    town: 'Paris',
    country: 'France',
    isFavorite: true
  },
  {
    id: 3,
    image: 'http://lorempixel.com/320/480/city/3',
    town: 'Ibiza',
    country: '‎Spain',
    isFavorite: false
  }
];

export const getFavorites = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(data);
    }, 1000);
  });
};
