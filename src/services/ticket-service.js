const data = [
  {
    id: 1,
    image: 'http://lorempixel.com/320/480/city/1',
    town: 'Bali',
    country: 'Indonesia',
    price_lable: 'from $99'
  },
  {
    id: 2,
    image: 'http://lorempixel.com/320/480/city/2',
    town: 'Paris',
    country: 'France',
    price_lable: 'from $109'
  },
  {
    id: 3,
    image: 'http://lorempixel.com/320/480/city/3',
    town: 'Ibiza',
    country: '‎Spain',
    price_lable: 'from $150'
  }
];

export const getTickets = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(data);
    }, 1000);
  });
};
