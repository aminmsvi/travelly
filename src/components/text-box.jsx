import React, { useState } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { Colors, Typography, Spacing } from '@styles';
import { Ionicons } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';

const TextBox = ({ placeholder, autoCompleteType, style }) => {
  const isPasswordInput = autoCompleteType === 'password';
  const [secureTextEntry, setSecureTextEntery] = useState(isPasswordInput);

  return (
    <View style={{ ...styles.container, ...style }}>
      <TextInput
        numberOfLines={1}
        placeholder={placeholder}
        autoCompleteType={autoCompleteType}
        secureTextEntry={secureTextEntry}
        style={styles.input}
      />
      {isPasswordInput ? (
        <TouchableOpacity onPress={() => setSecureTextEntery(!secureTextEntry)}>
          <Ionicons
            name={secureTextEntry ? 'md-eye' : 'md-eye-off'}
            size={24}
            color={Colors.GRAY_DARK}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.GRAY,
    borderRadius: Spacing.DEFAULT_RADIUS,
    paddingVertical: Spacing.SCALE_12,
    paddingHorizontal: Spacing.SCALE_24,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  input: {
    fontFamily: Typography.FONT_FAMILY_REGULAR,
    flexGrow: 1
  }
});

export default TextBox;
