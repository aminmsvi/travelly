import React from 'react';
import { SafeAreaView, ActivityIndicator } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const SceneLoading = () => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Colors.WHITE
      }}
    >
      <ActivityIndicator size="large" color={Colors.PRIMARY} />
    </SafeAreaView>
  );
};

export default SceneLoading;
