import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Spacing, Colors, Typography } from '@styles';

const ActionButton = ({ style = {}, onPress, text }) => {
  return (
    <TouchableOpacity style={{ ...styles.button, ...style }} onPress={onPress}>
      <Text style={styles.buttonText}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    borderRadius: Spacing.DEFAULT_RADIUS
  },
  buttonText: {
    padding: Spacing.SCALE_18,
    alignSelf: 'center',
    color: Colors.WHITE,
    fontFamily: Typography.FONT_FAMILY_BOLD
  }
});

export default ActionButton;
