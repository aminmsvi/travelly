import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Typography, Spacing } from '@styles';

const IconButton = ({ text, icon, onPress, style }) => {
  return (
    <TouchableOpacity style={style} onPress={onPress}>
      <View
        style={{
          paddingVertical: Spacing.SCALE_8,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center'
        }}
      >
        <MaterialCommunityIcons name={icon} size={28} color="#000" />
        <Text
          style={{
            paddingLeft: Spacing.SCALE_16,
            fontFamily: Typography.FONT_FAMILY_BOLD,
            fontSize: Typography.FONT_SIZE_14
          }}
        >
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default IconButton;
