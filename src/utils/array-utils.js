export const isEmpty = array => {
  return Array.isArray(array) && array.length == 0;
};

export const isNotEmpty = array => {
  return !isEmpty(array);
};
