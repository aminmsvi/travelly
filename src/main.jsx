import React, { useState } from 'react';
import * as Font from 'expo-font';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '@scenes/home';
import AuthScreen from '@scenes/auth';
import { AppLoading } from 'expo';

const getFonts = () =>
  Font.loadAsync({
    'roboto-regular': require('@assets/fonts/Roboto-Medium.ttf'),
    'roboto-bold': require('@assets/fonts/Roboto-Bold.ttf')
  });

const Stack = createStackNavigator();

const Main = () => {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  if (fontsLoaded) {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Auth" headerMode="none">
          <Stack.Screen name="Auth" component={AuthScreen} />
          <Stack.Screen name="Home" component={HomeScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  } else {
    return (
      <AppLoading startAsync={getFonts} onFinish={() => setFontsLoaded(true)} />
    );
  }
};

export default Main;
