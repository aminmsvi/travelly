import { scaleSize } from './mixins';

export const SCALE_64 = scaleSize(64);
export const SCALE_56 = scaleSize(56);
export const SCALE_48 = scaleSize(48);
export const SCALE_40 = scaleSize(40);
export const SCALE_32 = scaleSize(32);
export const SCALE_24 = scaleSize(24);
export const SCALE_18 = scaleSize(18);
export const SCALE_16 = scaleSize(16);
export const SCALE_12 = scaleSize(12);
export const SCALE_8 = scaleSize(8);
export const SCALE_4 = scaleSize(4);
export const SCALE_2 = scaleSize(2);

export const DEFAULT_RADIUS = SCALE_12;
export const TOOLBAR_HEIGHT = SCALE_56;
