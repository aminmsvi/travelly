export const PRIMARY = '#007BF8';
export const PRIMARY_DARK = '#0052A5';

export const WHITE = '#FFFFFF';
export const BLACK = '#000000';

// ACTIONS
export const SUCCESS = '#3adb76';
export const WARNING = '#ffae00';
export const ALERT = '#cc4b37';

// GRAYSCALE
export const GRAY = '#F4F4F4';
export const GRAY_DARK = '#d6d6d6';

export const LINK_BUTTON = PRIMARY;
