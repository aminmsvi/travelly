import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Colors, Spacing } from '@styles';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const HomeTabBar = ({ state, navigation }) => {
  return (
    <View style={{ backgroundColor: 'white' }}>
      <View style={styles.container}>
        {state.routes.map((route, index) => {
          const isFocused = state.index === index;

          let iconName = '';
          switch (route.name) {
            case 'Explore':
              iconName = isFocused ? 'home' : 'home-outline';
              break;
            case 'Favorites':
              iconName = isFocused ? 'heart' : 'heart-outline';
              break;
            case 'Profile':
              iconName = isFocused ? 'account' : 'account-outline';
              break;
            default:
              iconName = 'default';
              break;
          }

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          return (
            <TouchableOpacity onPress={onPress} key={route.name}>
              <MaterialCommunityIcons
                name={iconName}
                size={32}
                color={isFocused ? Colors.PRIMARY_DARK : Colors.GRAY_DARK}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    borderTopRightRadius: Spacing.SCALE_32,
    borderTopLeftRadius: Spacing.SCALE_32,
    borderColor: '#d6d6d6',
    borderWidth: 1,
    paddingBottom: Spacing.SCALE_16,
    paddingTop: Spacing.SCALE_18,
    backgroundColor: 'white'
  }
});

export default HomeTabBar;
