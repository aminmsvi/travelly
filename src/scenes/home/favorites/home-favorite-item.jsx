import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Typography, Spacing, Colors } from '@styles';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const HomeFavoriteItem = ({ item }) => {
  const [isFavorite, setIsFavorite] = useState(item.isFavorite);

  return (
    <View style={styles.container}>
      <Image source={{ uri: item.image }} style={styles.image} />
      <View style={{ flexGrow: 1, marginLeft: Spacing.SCALE_8 }}>
        <Text style={styles.townText}>{item.town}</Text>
        <Text style={styles.contryText}>{item.country}</Text>
      </View>
      <TouchableOpacity onPress={() => setIsFavorite(!isFavorite)}>
        <MaterialCommunityIcons
          name={isFavorite ? 'heart' : 'heart-outline'}
          size={24}
          color={isFavorite ? '#E6040F' : Colors.GRAY_DARK}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: Spacing.SCALE_8,
    paddingHorizontal: Spacing.SCALE_16,
    marginTop: Spacing.SCALE_8,
    marginLeft: Spacing.SCALE_24,
    marginRight: Spacing.SCALE_24,
    backgroundColor: Colors.GRAY,
    borderRadius: Spacing.DEFAULT_RADIUS,
    flexGrow: 1
  },
  image: {
    width: 56,
    height: 56,
    overflow: 'hidden',
    borderRadius: Spacing.DEFAULT_RADIUS
  },
  contryText: {
    fontFamily: Typography.FONT_FAMILY_REGULAR,
    fontSize: Typography.FONT_SIZE_12,
    color: Colors.GRAY_DARK
  },
  townText: {
    fontFamily: Typography.FONT_FAMILY_BOLD,
    fontSize: Typography.FONT_SIZE_14
  }
});

export default HomeFavoriteItem;
