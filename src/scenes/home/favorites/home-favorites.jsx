import React, { useState } from 'react';
import Constants from 'expo-constants';
import { Typography, Spacing, Colors } from '@styles';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { PlaceService } from '@services';
import { ArrayUtils } from '@utils';
import HomeFavoriteItem from './home-favorite-item';
import { SceneLoading } from '@components';

const HomeFavorites = () => {
  const [data, setData] = useState([]);

  PlaceService.getFavorites().then(places => setData(places));

  if (ArrayUtils.isNotEmpty(data)) {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.titleText}>Your{'\n'}favorites</Text>
          <TouchableOpacity>
            <Text style={styles.editButton}>Edit</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          data={data}
          ItemSeparatorComponent={() => (
            <View style={{ height: Spacing.SCALE_8 }} />
          )}
          contentContainerStyle={styles.favoritesList}
          showsVerticalScrollIndicator={false}
          keyExtractor={item => item.id.toString()}
          renderItem={({ item }) => <HomeFavoriteItem item={item} />}
        />
      </SafeAreaView>
    );
  } else {
    return <SceneLoading />;
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginTop: Constants.statusBarHeight
  },
  titleContainer: {
    paddingTop: Spacing.SCALE_56,
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingHorizontal: Spacing.SCALE_24
  },
  titleText: {
    fontSize: Typography.FONT_SIZE_24,
    fontFamily: Typography.FONT_FAMILY_BOLD,
    flexGrow: 1
  },
  editButton: {
    fontFamily: Typography.FONT_FAMILY_BOLD,
    color: Colors.LINK_BUTTON
  },
  favoritesList: {
    marginTop: Spacing.SCALE_24
  }
});

export default HomeFavorites;
