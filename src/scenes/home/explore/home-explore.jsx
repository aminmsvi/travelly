import React, { useState } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Constants from 'expo-constants';
import { Typography, Spacing, Colors } from '@styles';
import { TextBox } from '@components';
import { Ionicons } from '@expo/vector-icons';
import { TicketService } from '@services';
import { ArrayUtils } from '@utils';
import HomeExploreItem from './home-explore-item';
import HomeExploreRecommendedItem from './home-explore-recommended-item';
import { SceneLoading } from '@components';

const HomeExplore = () => {
  const [data, setData] = useState([]);

  TicketService.getTickets().then(tickets => setData(tickets));

  if (ArrayUtils.isNotEmpty(data)) {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.scrollContainer}
          showsVerticalScrollIndicator={false}
        >
          <Text style={styles.titleText}>Explore{'\n'}destination</Text>
          <View style={styles.searchContainer}>
            <TextBox style={{ flexGrow: 1 }} placeholder="Search anything..." />
            <View style={styles.settingIconContainer}>
              <TouchableOpacity>
                <Ionicons name="md-options" size={24} color="#adadad" />
              </TouchableOpacity>
            </View>
          </View>
          <FlatList
            horizontal={true}
            data={data}
            contentContainerStyle={styles.explorItemsContainerStyle}
            ItemSeparatorComponent={() => (
              <View style={{ width: Spacing.SCALE_16 }} />
            )}
            showsHorizontalScrollIndicator={false}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item }) => <HomeExploreItem item={item} />}
          />
          <Text style={styles.recommendedText}>Recommended</Text>
          <View
            style={{ flexDirection: 'column', paddingBottom: Spacing.SCALE_48 }}
          >
            {data.map(item => (
              <HomeExploreRecommendedItem
                key={item.id.toString()}
                item={item}
              />
            ))}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  } else {
    return <SceneLoading />;
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginTop: Constants.statusBarHeight
  },
  scrollContainer: {
    paddingTop: Spacing.SCALE_56
  },
  titleText: {
    fontSize: Typography.FONT_SIZE_24,
    fontFamily: Typography.FONT_FAMILY_BOLD,
    marginLeft: Spacing.SCALE_24
  },
  searchContainer: {
    marginLeft: Spacing.SCALE_24,
    marginRight: Spacing.SCALE_24,
    marginTop: Spacing.SCALE_16,
    flexDirection: 'row'
  },
  settingIconContainer: {
    backgroundColor: Colors.GRAY,
    borderRadius: Spacing.DEFAULT_RADIUS,
    paddingHorizontal: Spacing.SCALE_16,
    justifyContent: 'center',
    marginLeft: Spacing.SCALE_8
  },
  explorItemsContainerStyle: {
    paddingLeft: Spacing.SCALE_24,
    paddingRight: Spacing.SCALE_24,
    paddingTop: Spacing.SCALE_16
  },
  recommendedText: {
    fontFamily: Typography.FONT_FAMILY_BOLD,
    fontSize: Typography.FONT_SIZE_16,
    marginLeft: Spacing.SCALE_24,
    marginTop: Spacing.SCALE_16
  }
});

export default HomeExplore;
