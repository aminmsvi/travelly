import React from 'react';
import { View, Text, ImageBackground, StyleSheet } from 'react-native';
import { Typography, Spacing, Mixins } from '@styles';

const HomeExploreItem = ({ item }) => {
  return (
    <ImageBackground
      style={styles.imageBackground}
      source={{
        uri: item.image
      }}
    >
      <View style={styles.textsContainer} />
      <View
        style={{
          marginBottom: Spacing.SCALE_24
        }}
      >
        <Text style={styles.townText}>{item.town}</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingHorizontal: Spacing.SCALE_16
          }}
        >
          <Text style={styles.countryText}>{item.country}</Text>
          <Text style={styles.priceText}>{item.price_lable}</Text>
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  priceText: {
    fontFamily: Typography.FONT_FAMILY_REGULAR,
    color: 'white',
    opacity: 0.8,
    fontSize: Typography.FONT_SIZE_14
  },
  countryText: {
    fontFamily: Typography.FONT_FAMILY_REGULAR,
    color: 'white',
    opacity: 0.5,
    fontSize: Typography.FONT_SIZE_14
  },
  townText: {
    fontFamily: Typography.FONT_FAMILY_BOLD,
    color: 'white',
    fontSize: Typography.FONT_SIZE_20,
    marginLeft: Spacing.SCALE_16
  },
  imageBackground: {
    overflow: 'hidden',
    width: Mixins.scaleSize(240),
    height: Mixins.scaleSize(320),
    borderRadius: Spacing.DEFAULT_RADIUS,
    justifyContent: 'flex-end'
  },
  textsContainer: {
    position: 'absolute',
    opacity: 0.5,
    backgroundColor: 'black',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0
  }
});

export default HomeExploreItem;
