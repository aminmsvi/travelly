import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Typography, Spacing, Colors } from '@styles';

const HomeExploreRecommendedItem = ({ item }) => {
  return (
    <View style={styles.container}>
      <Image source={{ uri: item.image }} style={styles.image} />
      <View style={{ flexGrow: 1, marginLeft: Spacing.SCALE_8 }}>
        <Text style={styles.townText}>{item.town}</Text>
        <Text style={styles.text}>{item.country}</Text>
      </View>
      <Text style={styles.text}>{item.price_lable}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: Spacing.SCALE_8,
    paddingHorizontal: Spacing.SCALE_16,
    marginTop: Spacing.SCALE_8,
    marginLeft: Spacing.SCALE_24,
    marginRight: Spacing.SCALE_24,
    backgroundColor: Colors.GRAY,
    borderRadius: Spacing.DEFAULT_RADIUS,
    flexGrow: 1
  },
  image: {
    width: 56,
    height: 48,
    overflow: 'hidden',
    borderRadius: Spacing.DEFAULT_RADIUS
  },
  text: {
    fontFamily: Typography.FONT_FAMILY_REGULAR,
    fontSize: Typography.FONT_SIZE_12
  },
  townText: {
    fontFamily: Typography.FONT_FAMILY_BOLD,
    fontSize: Typography.FONT_SIZE_14
  }
});

export default HomeExploreRecommendedItem;
