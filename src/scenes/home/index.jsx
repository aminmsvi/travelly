import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeTabBar from './home-tab-bar';
import HomeExplore from './explore/home-explore';
import HomeFavorites from './favorites/home-favorites';
import HomeProfile from './profile/home-profile';

const Tab = createBottomTabNavigator();

const HomeScreen = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{ style: { backgroundColor: 'red' } }}
      tabBar={props => <HomeTabBar {...props} />}
    >
      <Tab.Screen name="Explore" component={HomeExplore} />
      <Tab.Screen name="Favorites" component={HomeFavorites} />
      <Tab.Screen name="Profile" component={HomeProfile} />
    </Tab.Navigator>
  );
};

export default HomeScreen;
