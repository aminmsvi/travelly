import React, { useState } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  ActivityIndicator
} from 'react-native';
import { IconButton, SceneLoading } from '@components';
import Constants from 'expo-constants';
import { Typography, Spacing, Mixins, Colors } from '@styles';
import { UserService } from '@services';
import { ObjectUtils } from '@utils';

const HomeProfile = () => {
  const [data, setData] = useState({});

  UserService.getProfile().then(profile => setData(profile));

  if (ObjectUtils.isNotEmpty(data)) {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
          paddingHorizontal: Spacing.SCALE_24,
          paddingTop: Constants.statusBarHeight
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingTop: Spacing.SCALE_56,
            paddingBottom: Spacing.SCALE_40
          }}
        >
          <Image
            style={{
              overflow: 'hidden',
              width: Mixins.scaleSize(60),
              height: Mixins.scaleSize(60),
              borderRadius: Mixins.scaleSize(60)
            }}
            source={{
              uri: data.avatar
            }}
          />
          <Text
            style={{
              fontSize: Typography.FONT_SIZE_24,
              fontFamily: Typography.FONT_FAMILY_BOLD,
              marginLeft: Spacing.SCALE_16
            }}
          >
            {data.firstName}
            {'\n'}
            {data.lastName}
          </Text>
        </View>
        <IconButton text="Your tickets" icon="ticket-outline" />
        <IconButton text="Payment methods" icon="credit-card" />
        <IconButton text="Contact support" icon="headset" />
        <IconButton text="Connect Facebook" icon="facebook" />
        <IconButton
          text="Default airport: Wave Hill Airport"
          icon="map-marker-outline"
        />
        <IconButton
          style={{
            flexGrow: 1,
            justifyContent: 'flex-end',
            marginBottom: Spacing.SCALE_32
          }}
          text="Log out"
          icon="logout"
        />
      </SafeAreaView>
    );
  } else {
    return <SceneLoading />;
  }
};

export default HomeProfile;
