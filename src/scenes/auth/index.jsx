import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AuthIntro from './auth-intro';
import AuthLogin from './auth-login';
import AuthSignUp from './auth-sign-up';

const Stack = createStackNavigator();

const AuthScreen = () => {
  return (
    <Stack.Navigator initialRouteName="Intro" headerMode="none">
      <Stack.Screen name="Intro" component={AuthIntro} />
      <Stack.Screen name="Login" component={AuthLogin} />
      <Stack.Screen name="SignUp" component={AuthSignUp} />
    </Stack.Navigator>
  );
};

export default AuthScreen;
