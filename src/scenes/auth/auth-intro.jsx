import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Typography, Spacing, Colors, Mixins } from '@styles';
import { ActionButton } from '@components';

const AuthIntro = ({ navigation }) => {
  const handleLogInPressed = () => {
    navigation.navigate('Login');
  };

  const handleSignUpPressed = () => {
    navigation.navigate('SignUp');
  };

  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require('@assets/images/sign-in.png')}
      />
      <Text style={styles.titleText}>Helping you{'\n'}travel more.</Text>
      <Text style={styles.descriptionText}>
        Our mission is to help you travel the world{'\n'}with ease. That's why
        we offer you{'\n'}the best prices for any ticket.
      </Text>
      <View style={{ flexDirection: 'row' }}>
        <ActionButton
          text="Sign up"
          onPress={handleSignUpPressed}
          style={styles.signUpButton}
        />
      </View>
      <View style={{ flexDirection: 'row' }}>
        <ActionButton
          text="Log in"
          onPress={handleLogInPressed}
          style={styles.loginButton}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: Spacing.SCALE_24,
    paddingHorizontal: Spacing.SCALE_32,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
    height: '100%'
  },
  image: {
    height: Mixins.scaleSize(300),
    width: Mixins.scaleSize(300),
    resizeMode: 'center'
  },
  titleText: {
    fontSize: Typography.FONT_SIZE_24,
    fontFamily: Typography.FONT_FAMILY_BOLD
  },
  descriptionText: {
    marginTop: Spacing.SCALE_12,
    fontSize: Typography.FONT_SIZE_14,
    textAlign: 'center',
    fontFamily: Typography.FONT_FAMILY_REGULAR
  },
  signUpButton: {
    marginTop: Spacing.SCALE_16,
    backgroundColor: Colors.PRIMARY_DARK,
    borderRadius: Spacing.DEFAULT_RADIUS,
    width: '100%'
  },
  loginButton: {
    marginTop: Spacing.SCALE_12,
    backgroundColor: Colors.PRIMARY,
    borderRadius: Spacing.DEFAULT_RADIUS,
    width: '100%'
  },
  buttonText: {
    padding: Spacing.SCALE_18,
    alignSelf: 'center',
    color: Colors.WHITE,
    fontFamily: Typography.FONT_FAMILY_BOLD
  }
});

export default AuthIntro;
