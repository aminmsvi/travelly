import React from 'react';
import { Typography, Spacing, Colors } from '@styles';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import { ActionButton, TextBox } from '@components';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';

const AuthLogin = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{ alignSelf: 'flex-start', marginTop: Spacing.SCALE_24 }}
        onPress={() => navigation.goBack()}
      >
        <Ionicons name="md-arrow-back" size={24} color="black" />
      </TouchableOpacity>
      <Text style={styles.titleText}>Log in</Text>
      <TextBox
        style={{ ...styles.input, marginTop: Spacing.SCALE_24 }}
        placeholder="email"
      />
      <TextBox
        style={{ ...styles.input, marginTop: Spacing.SCALE_8 }}
        autoCompleteType="password"
        secureTextEntry={true}
        placeholder="password"
      />
      <TouchableOpacity style={styles.forgotPasswordButton}>
        <Text style={styles.forgotPasswordText}>Forgot password?</Text>
      </TouchableOpacity>
      <View style={{ flexDirection: 'row' }}>
        <ActionButton
          style={styles.loginButton}
          text="Log in"
          onPress={() => navigation.navigate('Home')}
        />
      </View>
      <Text
        style={{
          marginTop: Spacing.SCALE_32,
          fontFamily: Typography.FONT_FAMILY_REGULAR
        }}
      >
        or continue with
      </Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: Spacing.SCALE_32
        }}
      >
        <TouchableOpacity
          style={{
            flex: 1,
            backgroundColor: '#3b5998',
            borderRadius: Spacing.DEFAULT_RADIUS,
            paddingVertical: Spacing.SCALE_12,
            marginRight: Spacing.SCALE_8
          }}
        >
          <FontAwesome
            style={{ alignSelf: 'center' }}
            name="facebook"
            size={32}
            color="white"
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 1,
            backgroundColor: Colors.GRAY,
            borderRadius: Spacing.DEFAULT_RADIUS,
            paddingVertical: Spacing.SCALE_12,
            marginLeft: Spacing.SCALE_8,
            alignItems: 'center'
          }}
        >
          <Image
            style={{ alignSelf: 'center', resizeMode: 'center' }}
            source={require('@assets/images/google-icon.png')}
            style={{ width: 32, height: 32 }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: Spacing.SCALE_24,
    paddingHorizontal: Spacing.SCALE_32,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
    height: '100%'
  },
  input: {
    width: '100%'
  },
  titleText: {
    fontSize: Typography.FONT_SIZE_24,
    fontFamily: Typography.FONT_FAMILY_BOLD,
    marginTop: Spacing.SCALE_32
  },
  loginButton: {
    marginTop: Spacing.SCALE_24,
    backgroundColor: Colors.PRIMARY_DARK,
    borderRadius: Spacing.DEFAULT_RADIUS,
    width: '100%'
  },
  buttonText: {
    padding: Spacing.SCALE_18,
    alignSelf: 'center',
    color: Colors.WHITE,
    fontFamily: Typography.FONT_FAMILY_BOLD
  },
  forgotPasswordText: {
    fontFamily: Typography.FONT_FAMILY_REGULAR,
    color: Colors.LINK_BUTTON
  },
  forgotPasswordButton: {
    alignSelf: 'flex-start',
    marginTop: Spacing.SCALE_12
  }
});

export default AuthLogin;
